import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {HomeComponent} from "./modules/home/home.component";

const routes: Routes = [
  {
    path: '',
    component: HomeComponent
  },
  {
    //404
    path: '**',
    redirectTo: ''
  },
  {
    //500
    path: 'error',
    redirectTo: ''
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
