import { Injectable } from '@angular/core';
import {Carousel} from "../../shared/models/carousel";

@Injectable({
  providedIn: 'root'
})
export class CarouselService {

  carousel : Carousel[] = [
    {
      id: 1,
      title: 'Qualifiées dans la douleur.',
      description:'En s\'imposant difficilement ce jeudi soir en Lituanie (83-75), l\'Équipe de France \n' +
        'a décroché son billet pour l\'Euro féminin 2023 qui se tiendra en Israël et en \n' +
        'Slovénie en juin prochain.',
      legend: 'Marie Joahnnes a brillé (21 points) - Photo : Julien Bacot / FFBB',
      image: 'http://www.ffbb.com/sites/default/files/whatsapp-image-2023-02-09-at-20.12.jpg',
      link: '',
      linkText: '',
      team: 'ÉQUIPE DE FRANCE MASCULINE',
      date: '09/02/2023'
    },
    {
      id: 2,
      title: 'Qualifiées dans la douleur. 2',
      description:'En s\'imposant difficilement ce jeudi soir en Lituanie (83-75), l\'Équipe de France \n' +
        'a décroché son billet pour l\'Euro féminin 2023 qui se tiendra en Israël et en \n' +
        'Slovénie en juin prochain.',
      legend: 'Marie Joahnnes a brillé (21 points) - Photo : Julien Bacot / FFBB',
      image: 'http://www.ffbb.com/sites/default/files/styles/detail_actu/public/322380302_700737428079495_7576455041475062038_n.jpg?itok=XlOC7HK8',
      link: '',
      linkText: '',
      team: 'ÉQUIPE DE FRANCE FÉMININE',
      date: '09/02/2023'
    },
    {
      id: 3,
      title: 'Qualifiées dans la douleur. 3',
      description:'En s\'imposant difficilement ce jeudi soir en Lituanie (83-75), l\'Équipe de France \n' +
        'a décroché son billet pour l\'Euro féminin 2023 qui se tiendra en Israël et en \n' +
        'Slovénie en juin prochain.',
      legend: 'Marie Joahnnes a brillé (21 points) - Photo : Julien Bacot / FFBB',
      image: 'http://www.ffbb.com/sites/default/files/cdf_troph_e_m_2021_2022_finale_toac_basket_pays_de_foug_res_0370.jpg',
      link: '',
      linkText: '',
      team: 'ÉQUIPE DE FRANCE FÉMININE',
      date: '09/02/2023'
    },
    {
      id: 4,
      title: 'Qualifiées dans la douleur. 4',
      description:'En s\'imposant difficilement ce jeudi soir en Lituanie (83-75), l\'Équipe de France \n' +
        'a décroché son billet pour l\'Euro féminin 2023 qui se tiendra en Israël et en \n' +
        'Slovénie en juin prochain.',
      legend: 'Marie Joahnnes a brillé (21 points) - Photo : Julien Bacot / FFBB',
      image: 'http://www.ffbb.com/sites/default/files/edf-f-annonce-prepa-2023.jpg',
      link: '',
      linkText: '',
      team: 'ÉQUIPE DE FRANCE FÉMININE',
      date: '09/02/2023'
    }
  ]
  constructor() { }

  getCarousel() {
    return this.carousel;
  }
}
