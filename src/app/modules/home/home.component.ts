import {Component, ViewChild, Renderer2} from '@angular/core';
import {QuestionService} from "../../core/services/question.service";
import {Question} from "../../shared/models/question";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent {
  questions: Question[] = [];
  q: Question = {} as Question;

  feedback!: any;
  check!: any;

  questionNumber: number = 1;

  constructor(private questionService: QuestionService, private renderer: Renderer2) {
    this.playHomeTest();
  }

  playHomeTest() {
    this.questions = this.questionService.homeQuestion();
    this.q = this.questions[0];
  }

  nextQuestion() {
    const nextQuestionButton = document.querySelector('.next-question');
    const ouiNonButton = document.querySelector('.reponse');
    const check = document.querySelector('.check');

    this.renderer.setStyle(nextQuestionButton, 'display', 'none');
    this.renderer.setStyle(ouiNonButton, 'display', 'block');
    this.renderer.removeClass(check, 'correct');
    this.renderer.removeClass(check, 'incorrect');

    if(this.questionNumber < 10) {
      this.questionNumber++;
      const progressionBar = document.querySelector('.progression');
      this.renderer.setStyle(progressionBar, 'width', `${this.questionNumber * 10}%`);
      this.q = this.questions[this.questionNumber - 1];
      this.feedback = '';
      this.check = '';

    }
  }

  checkAnswer(response: any, question: Question) {
    const nextQuestionButton = document.querySelector('.next-question');
    const ouiNonButton = document.querySelector('.reponse');
    const check = document.querySelector('.check');


    if (response.value === question.reponse[0]) {
      this.check = 'Bonne réponse !';
      this.feedback = question.feedback;
      this.renderer.addClass(check, 'correct')

    } else {
      this.check = 'Mauvais réponse !';
      this.feedback = question.feedback;
      this.renderer.addClass(check, 'incorrect')
      this.makeDivShake();
    }


    this.renderer.setStyle(ouiNonButton, 'display', 'none');
    this.renderer.setStyle(nextQuestionButton, 'display', 'block');
  }


  makeDivShake() {
    const quizzContainer = document.querySelector('.quizz-container');
    //make the div shake
    this.renderer.addClass(quizzContainer, 'shake');
    //remove the shake class after 1 second
    setTimeout(() => {
      this.renderer.removeClass(quizzContainer, 'shake');
    }, 350);
  }
}
