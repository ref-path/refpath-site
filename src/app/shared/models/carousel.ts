export interface Carousel {
  id: number;
  title: string;
  description: string;
  legend: string;
  image: string;
  link: string;
  linkText: string;
  team: string;
  date: string;
}
